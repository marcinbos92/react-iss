import React from 'react';

class TextComponent extends React.Component {

    render() {
        return (
            <p className="text">My text...</p>
        );
    }
}

export default TextComponent;